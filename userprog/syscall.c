#include "userprog/syscall.h"

void syscall_init(void) {
  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");

  // Sets up file system lockphore
  lock_init(&fs_lock, 1);
}
/************************************************************/
/**********IMPLEMENT SYSCALL HANDLER*************************/
/************************************************************/

static void syscall_handler(struct intr_f *f) {
	  uint32_t syscalls = mem_load(f, syscalls);
	  switch (syscalls) {
	  
			//Syscall OPEN
		  case SYS_OPEN: {
		    f->es = sys_open((const char *)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall CLOSE
		  case SYS_CLOSE: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    sys_close((int)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall CREATE
		  case SYS_CREATE: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_create((const char *)mem_load(f, ARG_0),
			    (unsigned int)mem_load(f, ARG_1));
		    return;
		  }
			//Syscall READ
		  case SYS_READ: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_read((int)mem_load(f, ARG_0),
			     (void *)mem_load(f, ARG_1),
		             (unsigned int)mem_load(f, ARG_2));
		    return;
		  }
			//Syscall WRITE
		  case SYS_WRITE: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_write((int)mem_load(f, ARG_0),
			     (const void *)mem_load(f, ARG_1),
		             (unsigned int)mem_load(f, ARG_2));
		    return;
		  }

			//Syscall EXIT
		  case SYS_EXIT: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    sys_exit((int)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall HALT
		  case SYS_HALT: {
		    sys_halt();
		    return;
		  }

			//Syscall TELL
		  case SYS_TELL: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_tell((int)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall SEEK
		  case SYS_SEEK: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    sys_seek((int)mem_load(f, ARG_0), (unsigned)mem_load(f, ARG_1));
		    return;
		  }
			//Syscall REMOVE
		  case SYS_REMOVE: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_remove((const char *)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall FILESIZE
		  case SYS_FILESIZE: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_filesize((int)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall EXEC
		  case SYS_EXEC: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_exec((const char *)mem_load(f, ARG_0));
		    return;
		  }
			//Syscall WAIT
		  case SYS_WAIT: {
			//FILLS ARGS WITH THE NEEDED ARGUMENTS
		    f->es = sys_wait((pid_t)mem_load(f, ARG_0));
		    return;
		  }
	  }
}
//Validate stack pointer then dereference the pointer
static uint32_t mem_load(struct intr_f *f, int offs) 
	{
	  if (get_u(f->esp + offs) == -1) {
	    sys_exit(EXIT_ERR);
	  }
	  return *(((uint32_t *)(f->esp + offs)));
	}
/************************************************************/
/**********IMPLEMENT THE SYSTEM CALLS************************/
/************************************************************/

/*Opens a file*/
int sys_open(const char *filename) {
  if (get_u((const uint8_t *)filename) == -1) 
	{
	    sys_exit(EXIT_ERR);
	  }

	  // Assign a number as a file descriptor
	  struct thread *thread = thread_current();
	  struct list_elem *node;

	  int f_desc = DESC_START;

	  // Find an unused file descriptor
	  for (node = list_begin(&thread->fmap); node != list_end(&thread->fmap);
	       node = list_next(node), f_desc++) {
	    struct fmap_t *ent = list_ent(node, struct fmap_t, ptr);
	    if (ent->f_desc > f_desc) {

	      node = list_next(node);
	      break;
	    }
	  }

	  if (f_desc < DESC_START) {
	    sys_exit(EXIT_ERR);
	  }
	  // Initialize memory for the file.
	  struct fmap_t *ent = malloc(sizeof(struct fmap_t));

	  if (ent == NULL) {
	    return -1;
	  }
	  ent->f_desc = f_desc;
	  lock_down(&fs_lock);
	  ent->file = filesys_open(filename);
	  lock_up(&fs_lock);

	  // Confirm file is opened.
	  if (ent->file == NULL) {
	    // Release the memory
	    free(ent);
	    return -1;
	  }

	  list_insert(node, &ent->ptr);
	  return f_desc;
	}


/*Closes opened file*/
void sys_close(int f_desc) 
	{

	  struct fmap_t *ent = find_fmap(f_desc);
	  if (ent == NULL) {
	    return;
	  }

	  lock_down(&fs_lock);
	  file_close(ent->file);
	  lock_up(&fs_lock);

	  list_remove(&ent->ptr);
	    // Release the memory
	  free(ent);
	}


/*Create new file*/
bool sys_create(const char *name, unsigned int size) 
	{

	  if (name == NULL) {
	    sys_exit(EXIT_ERR);
	  }
	  if (get_u((const uint8_t *)name) == -1) {
	    sys_exit(EXIT_ERR);
	  }
	  bool status;
	  lock_down(&fs_lock);
	  status = filesys_create(name, size);
	  lock_up(&fs_lock);
	  return status;
	}


/*Read file contents to a buffer*/
int sys_read(int f_desc, void *buffer, unsigned int size) 
	{

	  if (f_desc == STDIN_FILENO) {
	    unsigned int i;
	    for (i = 0; i < size; i++) {
	      if (!put_user((uint8_t *)buffer + i, (uint8_t)input_getc())) {
		sys_exit(EXIT_ERR);
	      }
	    }
	    return i;
	  }

	  if (get_u((const uint8_t *)buffer) == -1 ||
	      get_u((const uint8_t *)buffer + (size - 1)) == -1) {
	    sys_exit(EXIT_ERR);
	  }
	  struct fmap_t *ent = find_fmap(f_desc);
	  if (ent == NULL) {
	    sys_exit(EXIT_ERR);
	  }
	  lock_down(&fs_lock);
	  int read = file_read(ent->file, buffer, size);
	  lock_up(&fs_lock);
	  return read;
	}


/*Write buffer contents to a file*/
int sys_write(int f_desc, const void *buffer, unsigned int size) 
	{
	  if (get_u((const uint8_t *)buffer) == -1) {
	    sys_exit(EXIT_ERR);
	  }
	  if (f_desc == STDOUT_FILENO) {
	    putbuf((const char *)buffer, size);
	    return size;
	  }
	  struct fmap_t *ent = find_fmap(f_desc);

	  if (ent == NULL) {
	    sys_exit(EXIT_ERR);
	  }
	  lock_down(&fs_lock);
	  int written = file_write(ent->file, buffer, size);
	  lock_up(&fs_lock);
	  return written;
	}


/*EXIT syscall*/
void sys_exit(int status) 
	{
	  struct thread *curr = thread_current();
	  curr->exit_code = status;
	  thread_exit();
	}

/*HALT syscall*/
void sys_halt() 
	{ 
	  shutdown_power_off(); 
	}


/*TELL syscall*/
unsigned sys_tell(int f_desc) 
	{
	  struct fmap_t *ent = find_fmap(f_desc);
	  if (ent == NULL) {
	    sys_exit(EXIT_ERR);
	  }
	  lock_down(&fs_lock);
	  unsigned int loc = file_tell(ent->file);
	  lock_up(&fs_lock);
	  return loc;
	}


/*SEEK syscall*/
void sys_seek(int f_desc, unsigned pos) 
	{
	  struct fmap_t *ent = find_fmap(f_desc);
	  if (ent == NULL) {
	    sys_exit(EXIT_ERR);
	  }

	  lock_down(&fs_lock);
	  file_seek(ent->file, pos);
	  lock_up(&fs_lock);
	}


/*REMOVE syscall*/
bool sys_remove(const char *filename) 
	{
	  lock_down(&fs_lock);
	  bool success = filesys_remove(filename);
	  lock_up(&fs_lock);

	  return success;
	}

/*FILESIZE syscall*/
int sys_filesize(int f_desc) 
	{
	  struct fmap_t *ent = find_fmap(f_desc);
	  if (ent == NULL) {
	    sys_exit(EXIT_ERR);
	  }
	  lock_down(&fs_lock);
	  int size = file_size(ent->file);
	  lock_up(&fs_lock);
	  return size;
	}


/*EXEC syscall*/
pid_t sys_exec(const char *ivc) 
	{
	  if (get_u((const uint8_t *)ivc) == -1) {
	    sys_exit(EXIT_ERR);
	  }
	  return process_execute(ivc);
	}


/*WAIT syscall*/
int sys_wait(pid_t pid) 
	{
	  return process_wait(pid);
	}

/************************************************************/
/**************************MISC******************************/
/************************************************************/

static int get_u(const uint8_t *uaddr) {
  if ((uint32_t)uaddr >= (uint32_t)PHYS_BASE)
    return -1;
  int result;
  asm("movl $1f, %0; movzbl %1, %0; 1:" : "=&a"(result) : "m"(*uaddr));
  return result;
}

static bool put_user(uint8_t *udst, uint8_t byte) {
  int error_code;
  asm("movl $1f, %0; movb %b2, %1; 1:"
      : "=&a"(error_code), "=m"(*udst)
      : "q"(byte));
  return error_code != -1;
}

struct fmap_t *find_fmap(int f_desc) 
	{
	  //Assign a file descriptor
	  struct thread *thread = thread_current();
	  struct list_elem *node;
	  // Find unused descriptor
	  for (node = list_begin(&thread->fmap); node != list_end(&thread->fmap);
	       node = list_next(node), f_desc++) {
	    struct fmap_t *ent = list_ent(node, struct fmap_t, ptr);
	    if (ent->f_desc > f_desc) {
	      break;
	    }
	    if (ent->f_desc == f_desc) {
	      return ent;
	    }
	  }
	  return NULL;
	}

void close_all_files(struct thread *cur) {
  // Freeing up all the files
  for (struct list_elem *node = list_begin(&cur->fmap);
       node != list_end(&cur->fmap);) {

    struct fmap_t *fmap = list_ent(node, struct fmap_t, ptr);

    // We are about to remove the current node so we have to make sure we have
    // a reference to the next node
    node = list_next(node);

    lock_down(&fs_lock);
    file_close(fmap->file);
    lock_up(&fs_lock);

    list_remove(&fmap->ptr);

    // free up the memory
    free(fmap);
  }
}
