#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "lib/user/syscall.h"
#include "threads/synch.h"
#include "threads/thread.h"

typedef int pid_t;

/* File States */
enum load_status
  {
    NOT_LOADED,         /* Initial loading state. */
    LOAD_SUCCESS,       /* The file was loaded successfully with no issues. */
    LOAD_FAILED         /* The file failed to load. */
  };

/* Store Child Process */
struct process
  {
    struct list_elem elem;        /* List child process */
    pid_t pid;                    /* Thread identity */
    bool is_alive;                /* Process's start/stop status */
    bool is_waited;               /* Waiting status for process */
    int exit_status;              /* Exit status of the process */
    enum load_status load_status; /* Load status of file that is being executed */
    struct semaphore wait;        /* Wait for process to exit, then return its state */
    struct semaphore load;        /* Wait for the file to be loaded or failed to load */
  };


tid_t process_execute(const char *file_name);
int process_wait(tid_t);
void process_exit(void);
void process_activate(void);

#endif /* userprog/process.h */
